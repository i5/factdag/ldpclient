import { LdpResource } from "./LdpResource";
import { LinkHeaderMap } from "@i5/factlib-utils";
import { Stream } from "stream";
import { ILdpNonRdfSource } from "../interfaces/resources/ILdpNonRdfSource";

export class LdpNonRdfSource extends LdpResource implements ILdpNonRdfSource{

    private _content : Stream | string;
    private _describedByResource : LdpResource | null = null;
    /**
     * @param content  The binary content of the LDP NonRDF Source as stream or string.
     * @param slug  The slug is an optional hint by the client to the server for the intended name of a new resource.
     * @param uri  The URI of the resource. May contain a version parameter with a unix timestamp.
     * @param etag  ETAG as returned from a server.
     * @param linkHeaders A map containing link headers of the resource.
     */
    constructor(content : Stream | string, slug?: string, uri?: URL, etag?: string, linkHeaders?: LinkHeaderMap) {
        super("",slug,uri,etag,linkHeaders);
        this._content= content;
    }

    get describedByResource() : LdpResource | null{
        return this._describedByResource;
    }

    set describedByResource(describedByResource : LdpResource | null){
        this._describedByResource = describedByResource;
    }

    get binaryContent() : Stream | string{
        return this._content;
    }

    set binaryContent( content : Stream | string){
        this._content = content;
    }

    hasDescription() : boolean{
        return this._describedByResource != null;
    }
}