import { ILdpNonRdfSourceDescription } from "../interfaces/resources/ILdpNonRdfSourceDescription";
import { LinkHeaderMap } from "@i5/factlib-utils";
import { LdpResource } from "./LdpResource";

export class LdpNonRdfSourceDescription extends LdpResource implements ILdpNonRdfSourceDescription {
    private _describesBinaryURI: URL;
    private _existed: boolean;

    constructor(describesBinaryURI: URL,
                data: string | undefined,
                slug?: string,
                uri?: URL,
                etag?: string,
                linkHeaders?: LinkHeaderMap) {
        super(data, slug, uri, etag, linkHeaders);
        this._describesBinaryURI = describesBinaryURI;
        this._existed = data !== undefined;
    }

    public getDescribingBinaryURI(): URL {
        return this._describesBinaryURI
    }

    public descriptionExists(): boolean {
        return this._existed;
    }
}