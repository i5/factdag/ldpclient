import { Term } from "rdf-js";
import { logger } from "@i5/factlib-utils";
import { Namespaces } from "@i5/factlib-utils";
import { HeadersType, ILdpResource } from "../interfaces/resources/ILdpResource";
import { LinkHeaderMap, LinkHeaderMapEntry } from "@i5/factlib-utils";

// tslint:disable-next-line:no-var-requires
const rdf = require("rdflib");

export enum LdpResourceType {
    CONTAINER = "Container",
    RDF = "RDF Source",
    NONRDF = "Binary",
    UNKNOWN = "UNKNOWN",
}

export class LdpResource implements ILdpResource {

    /**
     * (Experimental) Creates a copy of an Ldp resource that will be stored at a different location as the original one.
     * It thereby updates triples to correctly reference to the new location.
     * @param newLoc The new location of the copy
     * @param resource The Ldp resource to copy
     */
    public static createResourceCopyAtLocation = (newLoc: string, resource: LdpResource): LdpResource => {
        const resourceUrlString = resource.uri.toString();
        const newUrl = new URL(newLoc);
        const sts = resource._store.statements.map((st: any) => {
            let { subject, predicate, object } = st;
            if (subject.value.indexOf(resourceUrlString) > -1) {
                const subjectValue = subject.value.replace(resourceUrlString, newLoc);
                subject = new rdf.sym(subjectValue);
            }
            if (predicate.value.indexOf(resourceUrlString) > -1) {
                const predicateValue = predicate.value.replace(resourceUrlString, newLoc);
                predicate = new rdf.sym(predicateValue);
            }
            if (object.value.indexOf(resourceUrlString) > -1) {
                const objectValue= object.value.replace(resourceUrlString, newLoc);
                object = new rdf.sym(objectValue);
            }
            return new rdf.Statement(subject,predicate,object);
        });
        const newStore = rdf.graph();
        newStore.addAll(sts);
        const content = rdf.serialize(undefined, newStore, newUrl.origin + "/", "text/turtle").replace("<>", "</>");
        return new LdpResource(content,resource.slug, newUrl, resource.etag, resource._linkHeaderMap);
    }

    protected _store: any;
    protected _uri: URL;
    protected _logger = logger;
    protected _linkHeaderMap = new LinkHeaderMap();
    private _slug: string;
    private _etag: string;

    private _headers: HeadersType = {};

    get headers() {
        return this._headers;
    }

    set headers(headers: HeadersType) {
        this._headers = headers;
    }

    get etag(): string {
        return this._etag;
    }

    get uri(): URL {
        return new URL(this._uri.toString());
    }

    get resourceURI(): URL {
        const url = new URL(this._uri.origin);
        url.pathname = this._uri.pathname;
        return url;
    }

    get slug(): string {
        return this._slug;
    }

    public get store(): any {
        return this._store;
    }

    get strongEtag(): string {
        if (this.etag && this.etag[0] === "W") {
            return this._etag.slice(2, this._etag.length);
        } else {
            return this._etag;
        }

    }
    /**
     * Determines the type of the LDP Resource
     * @returns The type of the LDP Resource
     */

    public get type(): LdpResourceType {
        const typeLinkHeaders = this.getLinkHeaders("type").map((url) => url.toString());
        const isRDFResource = typeLinkHeaders.indexOf(Namespaces.LDP(Namespaces.LDP_TYPES.RDF_SOURCE).value) > -1;
        if (isRDFResource) {
            const isContainer = typeLinkHeaders.indexOf(Namespaces.LDP(Namespaces.LDP_TYPES.CONTAINER).value) > -1;
            return isContainer ? LdpResourceType.CONTAINER : LdpResourceType.RDF;
        } else {
            // Missing RDFSource link header. We could first check whether the NonRDFSource link header is present
            // but we can directly assume that the resource must be a NonRDFSource
            return LdpResourceType.NONRDF;
        }
    }

    /**
     * @param data  A RDF string containing the data of the resource.
     * @param slug  The slug is an optional hint by the client to the server for the intended name of a new resource.
     * @param uri  The URI of the resource. May contain a version parameter with a unix timestamp.
     * @param etag  ETAG as returned from a server.
     * @param linkHeaders A map containing link headers of the resource.
     */
    constructor(data?: string, slug?: string, uri?: URL, etag?: string, linkHeaders?: LinkHeaderMap) {
        this._store = rdf.graph();
        this._slug = "";
        this._etag = "";
        if (uri === undefined) {
            this._uri = new URL("http://127.0.0.1/"); // TODO IS A PROBLEM
        } else {
            this._uri = uri;
        }
        if (etag) {
            this._etag = etag;
        }
        if (slug) {
            this._slug = slug;
        }
        if (linkHeaders) {
            this._linkHeaderMap = linkHeaders;
        }
        if (data && uri !== undefined) {
            rdf.parse(data, this._store, this.resourceURI.toString(), "text/turtle");
        }
    }

    public getLinkHeaders(key: string): LinkHeaderMapEntry<URL> {
        return this._linkHeaderMap.get(key);
    }

    public queryStore(predicate: Term | RegExp, object?: Term | RegExp, subject?: Term | RegExp) {
        let res;
        if (subject) {
            res = this._store.sym(subject);
        } else {
            res = this._store.sym(this.resourceURI.toString());
        }
        return this._store.match(res, predicate, object);
    }

    public serialize(base?: string): string {
        if (this._uri === undefined) {
            return "";
        } else {
            return rdf.serialize(undefined, this._store, base || this._uri.origin + "/", "text/turtle").replace("<>", "</>");
        }
    }
}
