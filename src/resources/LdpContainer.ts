import { LdpResource } from "./LdpResource";
import { Namespaces } from "@i5/factlib-utils";
import { ILdpContainer } from "../interfaces/resources/ILdpContainer";

export class LdpContainer extends LdpResource implements ILdpContainer {
    getContainedResourceList(): string[] {
        const rdfResult = this.queryStore(Namespaces.LDP(Namespaces.LDP_RELATION.CONTAINS));
        const result = [];
        for (const item of rdfResult) {
            result.push(item.object.value);
        }
        return result;
    }
}