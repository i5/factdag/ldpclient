import { ILdpContainer } from "../interfaces/resources/ILdpContainer";
import { ILdpNonRdfSource } from "../interfaces/resources/ILdpNonRdfSource";
import { ILdpNonRdfSourceDescription } from "../interfaces/resources/ILdpNonRdfSourceDescription";

/**
 * The interface of a generic factory that creates LDP resources from HTTP responses.
 */
export interface ILDPResourceFactory<TResource, TResponse>{

    /**
     * Creates and returns an LDP RDF Source from an HTTP response
     * @param response An  HTTP response from which the LDP resource should be created.
     * @param uri The URI of the LDP resource
     */
    createResource(response: TResponse, uri : URL): Promise<TResource>;

    /**
     * Creates and returns an LDP Container from an HTTP response
     * @param response An HTTP response from which the LDP container should be created
     * @param uri The URI of the LDP Container
     */
    createContainer(response: TResponse, uri: URL): Promise<TResource & ILdpContainer>;

    /**
     * Creates and returns and LDP Non RDF Source from an HTTP response
     * @param response An HTTP response from which the LDP NonRDF Source shoulb be created
     * @param uri The URI of the LDP Non RDF Source
     * @param describedByResource An optional LDP RDF Source that describes the LDP Non RDF Source
     */
    createBinaryResource(response: TResponse, uri : URL, describedByResource : TResource | null): Promise<TResource & ILdpNonRdfSource>;

    /**
     * Creates and returns an object that represents a describing LDP RDF Source of an LDP NonRDF Source
     * @param response An HTTP response of the describing resource from which the LDP RDF Source shoulb be created
     * @param uri The URI of the describing LDP RDF Source
     * @param binaryURI The URI of the described LDP NonRDF Source
     */
    createBinaryDescriptionResource(response : TResponse | undefined, uri: URL | undefined, binaryURI: URL): Promise<TResource & ILdpNonRdfSourceDescription>;
}
