import { ILDPResourceFactory } from "./ILDPResourceFactory";
import { LdpResource } from "../resources/LdpResource";
import { LinkHeaderMap } from "@i5/factlib-utils";
import { Namespaces } from "@i5/factlib-utils";
import { TYPE, buildLinkHeadersMap } from "@i5/factlib-utils";
import { LdpContainer } from "../resources/LdpContainer";
import { HeadersType } from "../interfaces/resources/ILdpResource";
import { LdpNonRdfSource } from "../resources/LdpNonRdfSource";
import { AxiosResponse } from "axios";
import { Stream } from "stream";
import { LdpNonRdfSourceDescription } from "../resources/LdpNonRdfSourceDescription";

export type AxiosResponseWithURL<T = any> = AxiosResponse<T> & { url: URL }

export class AxiosLDPResourceFactory implements ILDPResourceFactory<LdpResource, AxiosResponseWithURL>{

    /**
     * Checks whether an LDP resource is a container based on its link headers.
     * @param responseLinkHeaderMap The link headers of an LDP resource.
     * @returns True if the link headers specify an LDP resource as a container.
     */
    public static isContainer(responseLinkHeaderMap: LinkHeaderMap): boolean {
        const typeLinksAsStrings = responseLinkHeaderMap.get(TYPE).map(url => url.toString());
        const containerURIasString = Namespaces.LDP(Namespaces.LDP_TYPES.CONTAINER).value;
        return typeLinksAsStrings.indexOf(containerURIasString) > -1;
    }

    public async createContainer(response: AxiosResponse<string>, uri: URL): Promise<LdpContainer> {
        const data = response.data;

        const linkHeaderString = response.headers.link || "";
        const linkHeaders = buildLinkHeadersMap(linkHeaderString);

        const headers: HeadersType = {
            ...response.headers,
        };

        const etag = response.headers.etag || null;
        const container = new LdpContainer(data, "", uri, etag, linkHeaders);
        container.headers = headers;

        return container;
    }

    public async createResource(response: AxiosResponse<string>, uri: URL): Promise<LdpResource> {
        const linkHeaderString = response.headers.link || "";
        const linkHeaders = buildLinkHeadersMap(linkHeaderString);
        if (AxiosLDPResourceFactory.isContainer(linkHeaders)) {
            return this.createContainer(response, uri);
        } else {
            const headers: HeadersType = {
                ...response.headers,
            };
            const data = response.data;
            const etag = response.headers.etag || null;
            const resource = new LdpResource(data, "", uri, etag, linkHeaders);
            resource.headers = headers;
            return resource;
        }
    }

    public async createBinaryResource<T = string | Stream>(response: AxiosResponse<T>, uri: URL, describedByResource: LdpResource | null) {
        const linkHeaderString = response.headers.link || "";
        const linkHeaders = buildLinkHeadersMap(linkHeaderString);
        const etag = response.headers.etag || null;
        const binary = new LdpNonRdfSource(response.data as any, "", uri, etag, linkHeaders);
        binary.headers = response.headers;
        if (describedByResource) {
            binary.describedByResource = describedByResource;
        }
        return binary;
    }

    public async createBinaryDescriptionResource(response: AxiosResponseWithURL<any> | undefined, uri: URL | undefined, binaryURI: URL): Promise<LdpNonRdfSourceDescription> {
        if (response) {
            const headers: HeadersType = {
                ...response.headers,
            };

            const linkHeaderString = response.headers.link || "";
            const linkHeaders = buildLinkHeadersMap(linkHeaderString);
            const data = response.data;
            const etag = response.headers.etag || null;

            const resource = new LdpNonRdfSourceDescription(binaryURI, data, "", uri, etag, linkHeaders);
            resource.headers = headers;
            return resource;
        } else {
            return new LdpNonRdfSourceDescription(binaryURI, undefined, "", uri);
        }
    }
}