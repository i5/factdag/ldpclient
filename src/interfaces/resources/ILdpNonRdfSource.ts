import { ILdpResource } from "./ILdpResource";
import { Stream } from "stream";

/**
 * An interface that extends the [[ILdpResource]] interface with features for LDP NonRDF Sources.
 */
export interface ILdpNonRdfSource extends ILdpResource{
    /**
     * The binary content of the LDP NonRDF Source
     */
    binaryContent : Stream | string;
    /**
     * An optional LDP RDF Source that describes the LDP NonRDF Source that is represented by this instance.
     */
    describedByResource : ILdpResource | null;

    /**
     * Determines if the LDP NonRDF Source is described by an LDP RDF Source
     * @returns True iff the LDP NonRDF Source is described by an LDP RDF Source.
     */
    hasDescription(): boolean;
}