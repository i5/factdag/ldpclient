import { ILdpResource } from "./ILdpResource";

/**
 * An interface that extends the [[ILdpResource]] interface with features for LDP Containers.
 */
export interface ILdpContainer extends ILdpResource {
    /**
     * Retrieves the resources contained in the container from the local store.
     * @return An array containing the URIs as strings of all the resources that are contained in the container.
     */
    getContainedResourceList(): string[];
}

/**
 * Tests whether a given object represents an LDP Container, i.e. that implements [[ILdpContainer]]
 * @param object The object that is tested to be an [[ILdpContainer]]
 */
export function isLdpContainer(object: any): object is ILdpContainer {
    return typeof object.getContainedResourceList === "function";
}
