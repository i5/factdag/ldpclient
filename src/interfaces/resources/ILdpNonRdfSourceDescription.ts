import { ILdpResource } from "./ILdpResource";

/**
 * An interface that extends the [[ILdpResource]] interface with features for LDP RDF Sources that describe LDP NonRDF Sources.
 */
export interface ILdpNonRdfSourceDescription extends ILdpResource{
    /**
     * @returns the URI of the Ldp NonRDF Source the description is describing.
     */
    getDescribingBinaryURI() : URL;

    /**
     * Tests whether the description exists
     */
    descriptionExists() : boolean
}