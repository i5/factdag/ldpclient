import { Term } from "rdf-js";
import { LinkHeaderMapEntry } from "@i5/factlib-utils";

export type HeadersType = Record<string, string>;

/**
 * The interface of a mutable LDP Resource
 */
export interface ILdpResource {
    /**
     * A hint to the server which identifier should be used when this LDP Resource is newly created at the server.
     */
    readonly slug: string;

    /**
     * The weak etag of the LDP Resource
     */
    readonly etag: string;

    /**
     * The uri of the LDP Resource
     */
    readonly uri: URL;

    readonly resourceURI: URL;
    /**
     * The strong etag of the LDP Resource
     */
    readonly strongEtag: string;
    /**
     * The instance of rdf graph store that can be used to alter an LDP resource
     */
    readonly store: any;

    /**
     * The original HTTP Headers of this resource as received from the LDP server.
     */
    readonly headers: HeadersType;

    /**
     * Returns an iterable object of link headers with a specific "rel" value
     * @param key The "rel" value of the wanted link header(s)
     */
    getLinkHeaders(key: string): LinkHeaderMapEntry<URL>;

    /**
     * Queries the local RDF store.
     */
    queryStore(predicate: Term | RegExp, object?: Term | RegExp, subject?: Term | RegExp): any;

    /**
     * Serializes the content of the RDF store.
     * @returns A string containing a Turtle serialization of the store and
     * reformats it to be compatible with Trellis.
     */
    serialize(): string;
}
