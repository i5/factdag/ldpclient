import { Stream } from "stream";
import { ILdpContainer } from "./resources/ILdpContainer";
import { ILdpNonRdfSource } from "./resources/ILdpNonRdfSource";
import { ILdpNonRdfSourceDescription } from "./resources/ILdpNonRdfSourceDescription";
import { HeadersType, ILdpResource } from "./resources/ILdpResource";

/**
 * An interface describing the methods that an LDPClient implementation must provide
 * that handles the the low-level interaction with a Linked Data Platform.
 */
export interface ILdpClient {

    /**
     * Sends an HTTP-Head request to the given URL.
     * @param uri The url to which the HTTP-Head request is sent.
     * @param headers Optional HTTP-Headers for the HTTP-Head request
     * @returns The response of the request as returned by the server
     */
    httpHeadResource(url: URL, headers?: HeadersType): Promise<any>;

    /**
     * Sends an HTTP-Get request to the given URL.
     * @param uri The url to which the HTTP-Get request is sent.
     * @param headers Optional HTTP-Headers for the HTTP-Get request
     * @returns The response of the request as returned by the server
     */
    httpGetResource(url: URL, headers?: HeadersType): Promise<any>;

    /**
     * Sends an HTTP-Put request to the given URL.
     * @param uri The url to which the HTTP-Put request is sent.
     * @param data The body payload of the HTTP-Put request as a stream or string.
     * @param headers Optional HTTP-Headers for the HTTP-Put request
     * @returns The response of the request as returned by the server
     */
    httpPutResource(url: URL, data: string | Stream, headers?: HeadersType): Promise<any>;

    /**
     * Sends an HTTP-Post request to the given URL.
     * @param uri The url to which the HTTP-Post request is sent.
     * @param data The body payload of the HTTP-Post request as a stream or string.
     * @param headers Optional HTTP-Headers for the HTTP-Post request
     * @returns The response of the request as returned by the server
     */
    httpPostResource(
        url: URL,
        data: string | Stream,
        headers?: HeadersType,
        onUploadProgress?: (progressEvent: any) => void,
    ): Promise<any>;

    // Retrieve Data from LDP

    /**
     * Retrieves an LDP RDF Source from a given URI.
     * @param uri The URI of the resource.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @returns An instance that implements [[ILdpResource]] and represents the requested LDP RDF Source
     */
    getResource(uri: URL, headers?: HeadersType): Promise<ILdpResource>;

    /**
     * Retrieves an LDP container from a given URI.
     * @param uri The URI of an LDP container.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @returns An instance that implements [[ILdpContainer]] and represents the requested LDP Container
     */
    getContainer(uri: URL, headers?: HeadersType): Promise<ILdpContainer>;

    /**
     * Retrieve an LDP NonRDF Source from a given URI.
     * @param uri The URI of the resource.
     * @param acceptContentType The accept content type of the binary content.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @returns An instance that implements [[ILdpNonRdfSource]] and represents the requested LDP NonRDF Source
     */

    getLDPNonRDFSource(uri: URL, acceptContentType: string, headers?: HeadersType): Promise<ILdpNonRdfSource>;

    /**
     * Tries to retrieve an LDP RDF Source description of an LDP NonRDF Source from a given URI.
     * @param uri The URI of the resource.
     * @param acceptContentType The accept content type of the binary content.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @returns An instance that implements [[ILdpNonRdfSourceDescription]]. If the description exists, the
     *          RDF store of the instance will contain the tripples of the describing LDP RDF Source.
     */

    getLDPNonRDFSourceDescription(
        uri: URL,
        acceptContentType: string,
        headers?: HeadersType,
    ): Promise<ILdpNonRdfSourceDescription>;

    // Create LDP Resources

    /**
     * Creates a new LDP RDF Source in a given LDP Container.
     * @param location An LDP container in which the resource is to be placed.
     * The container can be identified by a container object or directly by a URI.
     * @param payload The content of the LDP resource that is to be stored at the given location.
     * @param slug A hint to the server which identifier should be used for the new LDP RDF Source.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @returns An instance that implements [[ILdpResource]] and represents the requested LDP RDF Source
     */
    createLDPRDFSource(
        location: ILdpContainer | URL,
        payload: string,
        slug: string,
        headers?: HeadersType,
    ): Promise<ILdpResource>;

    /**
     * Creates a new LDP container in a given LDP Container.
     * @param location An LDP container in which the new container is to be placed.
     * The container can be identified by a container object or directly by a URI.
     * @param slug A hint to the server which identifier should be used for the new container.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @returns An instance that implements [[ILdpContainer]] and represents the created LDP Container
     */
    createLDPContainer(location: ILdpContainer | URL, slug: string, headers?: HeadersType): Promise<ILdpContainer>;

    /**
     * Creates a new LDP NonRDF Source in a given LDP Container.
     * @param location An LDP container in which the new LDP NonRDF Source is to be placed.
     * The container can be identified by a container object or directly by a URI.
     * @param slug A hint to the server which identifier should be used for the new LDP NonRDF Source.
     * @param content The binary content of the LDP NonRDF Source. Can be a string or a stream.
     * @param contentType The content-type of the binary content, e.g. "image/png" for a png image.
     * @param headers Optional HTTP-Headers that are additionally included in the HTTP requests
     * @returns An instance that implements [[ILdpNonRdfSource]] and represents the created LDP NonRDF Source
     */
    createLDPNonRDFSource<T = string | Stream>(
        location: ILdpContainer | URL,
        slug: string,
        content: T,
        contentType?: string,
        headers?: HeadersType,
    ): Promise<ILdpNonRdfSource>;

    /**
     * Deletes an LDP resource. In the case of an LDP container it will not
     * recursively delete the contained LDP resources.
     *
     * @param uri The uri of the LDP resource to be deleted
     * @param headers Optional HTTP-Headers that are additionally included in the HTTP requests
     * @returns The HTTP response as returned by the LDP server
     */
    deleteLDPResource(uri: URL, headers?: HeadersType): Promise<any>;

    // Update LDP Resources

    /**
     * Updates and stores the content of an LDP Resource at the server.
     * @param ldpResource The LDP Resource to be updated
     * @param headers Optional HTTP-Headers that are additionally included in the HTTP requests
     * @returns The updated LDP resource
     */
    updateLdpRDFSource(ldpResource: ILdpResource, headers?: HeadersType): Promise<ILdpResource>;

    /**
     * Updates an LDP NonRDF source.
     * @param ldpNonRDFResource The LDP NonRDF Source to be updated
     * @param content The binary content to which the resource should be updated as string or stream
     * @param contentType The content-type of the binary content, e.g. "image/png" for a png image.
     * @param headers Optional HTTP-Headers that are additionally included in the HTTP requests
     * @returns The updated LDP NonRDF Source
     */
    updateLdpNonRDFSource<T = string | Stream>(
        ldpNonRDFResource: ILdpNonRdfSource,
        content: T,
        contentType: string,
        headers?: HeadersType,
    ): Promise<ILdpNonRdfSource>;

    /**
     * Checks if an LDP resource exists.
     * @param uri URI of an LDP resource.
     * @return True if the resource exists.
     */
    isResourceExistent(uri: URL): Promise<boolean>;

    /**
     * Checks if a resource at a given URL is binary.
     * @param uri URI of an LDP resource.
     * @return True if the resource is binary.
     */
    isResourceBinary(uri: URL): Promise<boolean>;

}
