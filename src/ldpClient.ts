import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";

import { Stream } from "stream";
import { buildLinkHeadersMap, DESCRIBED_BY, logger, TYPE } from "@i5/factlib-utils";
import { AxiosResponseWithURL } from "./factory/AxiosLDPResourceFactory";
import { ILDPResourceFactory } from "./factory/ILDPResourceFactory";
import { ILdpClient } from "./interfaces/ILdpClient";
import { ILdpContainer, isLdpContainer } from "./interfaces/resources/ILdpContainer";
import { ILdpNonRdfSource } from "./interfaces/resources/ILdpNonRdfSource";
import { HeadersType, ILdpResource } from "./interfaces/resources/ILdpResource";

class LdpClientError extends Error {
    public response: any;
    public status: number;

    constructor(statusText: string, status: number, response: any) {
        super(`HTTP-Status-Code: ${status}: ${statusText}`);
        this.response = response;
        this.status = status;
    }
}

export type SupportedAxiosResponseType = "stream" | "text" | "blob";

// tslint:disable-next-line: max-classes-per-file
export class AxiosLDPClient<TResource extends ILdpResource> implements ILdpClient {

    public static getURIFromLocation(location: URL | ILdpContainer): URL {
        if (isLdpContainer(location)) {
            return location.uri;
        } else {
            return location;
        }
    }

    /**
     * An Axios instance that is used as the HTTP client for communicating with LDP servers.
     */
    protected service: AxiosInstance;

    /**
     * A Winston logger instance used to log information.
     */
    protected logger: any = logger;

    /**
     * Default headers that are used to retrieve a resource from an LDP server.
     */
    private _defaultLDPHeaders: Record<string, string> = {
        "Accept": "text/turtle",
        "Cache-Control": "no-cache",
        "Content-Type": "text/turtle",
    };

    /**
     * Allows to proxy the requests
     */
    private _proxyUrlString = "";

    /**
     *
     * @param _factory The factory that will be used to create LDP resources
     * @param loggerImpl The logger that is used in the client
     */
    constructor(
        private _factory: ILDPResourceFactory<TResource, AxiosResponse>,
        loggerImpl?: any,
        axiosInstance?: AxiosInstance,
    ) {
        if (loggerImpl) {
            this.logger = loggerImpl;
        }
        const service = axiosInstance || axios.create({
            headers: this._defaultLDPHeaders,
        });
        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    }

    /**
     * Sets the logger.
     * @param logger Impl
     */
    public setLogger(loggerImpl: any) {
        this.logger = loggerImpl;
    }

    /**
     * Sets the factory that is used to create LDP resources
     * @param factory The factory for LDP resources
     */
    public setFactory(factory: ILDPResourceFactory<TResource, AxiosResponseWithURL>) {
        this._factory = factory;
    }

    /**
     * Sets the proxy URL
     * @param proxyUrlString The proxy URL to use for the requests
     */
    public setProxyUrlString(proxyUrlString: string): void {
        this._proxyUrlString = proxyUrlString;
    }

    // Lower level HTTP / LDP specific methods

    public async httpGetResource<T = any>(
        uri: URL,
        headers: HeadersType = {},
        responseType: SupportedAxiosResponseType = "text",
    ): Promise<AxiosResponse<T>> {
        this.logger.verbose("HTTP GET (%s)", uri.toString());
        try {
            const response = await this.service.request(this.createAxiosRequest(uri, {
                method: "get",
                responseType,
            }, headers));

            if (response.status > 400 && response.status < 600) {
                throw new LdpClientError(response.statusText, response.status, response);
            }

            return response;
        } catch (err) {
            this.logger.error("Can't get Resource '%s': %s", uri.toString(), err);
            throw err;
        }
    }

    public async httpHeadResource(uri: URL, headers: HeadersType = {}): Promise<AxiosResponse> {
        this.logger.verbose("HTTP HEAD (%s)", uri.toString());
        try {
            const headResponse = await this.service.request(this.createAxiosRequest(uri, {
                method: "HEAD",
            }, headers));
            return headResponse;
        } catch (err) {
            this.logger.error("Error during HTTP-Head: %s", err.toString());
            throw err;
        }
    }

    public async httpPostResource<T = string | Stream>(
        uri: URL,
        data: T,
        headers: HeadersType = {},
        onUploadProgress?: (event: any) => void): Promise<AxiosResponse<T>> {
        try {
            const response = await this.service.request<T>(this.createAxiosRequest(uri, {
                data,
                method: "POST",
                onUploadProgress,
            }, headers));
            return response;
        } catch (err) {
            this.logger.error("Error during HTTP-Post: %s", err.toString());
            if (err.response) {
                const response = err.response;
                if (response.status > 400 && response.status < 600) {
                    throw new LdpClientError(response.statusText, response.status, response);
                }
            }
            throw err;
        }
    }

    public async httpPutResource<T = string | Stream>(
        uri: URL,
        data: T,
        headers: HeadersType): Promise<AxiosResponse<T>> {
        try {
            const response = await this.service.request(this.createAxiosRequest(uri, {
                data,
                method: "PUT",
            }, headers));
            return response;
        } catch (err) {
            this.logger.error("Error during HTTP-Put: %s", err.toString());
            if (err.response) {
                const response = err.response;
                if (response.status > 400 && response.status < 600) {
                    throw new LdpClientError(response.statusText, response.status, response);
                }
            }
            throw err;
        }
    }
    // Get / Read operations

    public async getResource(url: URL, headers: HeadersType = {}): Promise<TResource> {
        try {
            const response = await this.httpGetResource<string>(url, headers);
            return await this._factory.createResource(response, url);
        } catch (err) {
            this.logger.error(`Error while getting the LDP resource %s : %s`, url.toString(), err);
            throw err;
        }
    }

    public async getContainer(url: URL, headers: HeadersType = {}): Promise<TResource & ILdpContainer> {
        try {
            const response = await this.httpGetResource<string>(url, headers);
            return await this._factory.createContainer(response, url);
        } catch (err) {
            this.logger.error(`Error while getting the LDP container %s: %s`, url.toString(), err);
            throw err;
        }
    }

    /**
     * Retrieve an LDP NonRDF Source from a given URI.
     * @param uri The URI of the resource.
     * @param acceptContentType The accept content type of the binary content.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @param responseType The response type of the axios response.
     * Use "stream" to get a [[Stream]] or "text" to get a string. Default is "stream"
     * @returns An instance that implements [[ILdpNonRdfSource]] and represents the requested LDP NonRDF Source
     */
    public async getLDPNonRDFSource<T = string | Stream>(
        url: URL,
        acceptContentType: string = "",
        headers: HeadersType = {},
        responseType: SupportedAxiosResponseType = "stream",
    ): Promise<TResource & ILdpNonRdfSource> {
        try {
            const request = this.createAxiosRequest(url, {
                method: "get",
                responseType,
            }, {
                Accept: acceptContentType,
                ...headers,
            });
            const response: AxiosResponse<T> = await this.service.request(request);
            const linkHeaderString = response.headers.link || "";
            const linkHeaders = buildLinkHeadersMap(linkHeaderString);
            const describedBy = linkHeaders.getFirstLink(DESCRIBED_BY);
            const describedByResource = describedBy ? await this.getResource(describedBy) : null;
            return this._factory.createBinaryResource(response, url, describedByResource);
        } catch (err) {
            throw err;
        }
    }

    /**
     * Tries to retrieve an LDP RDF Source description of an LDP NonRDF Source from a given URI.
     * @param uri The URI of the resource.
     * @param acceptContentType The accept content type of the binary content.
     * @param headers Optional additional headers for the HTTP request that the LDP client will send
     * @param responseType The response type of the axios response.
     * Use "stream" to get a [[Stream]] or "text" to get a string. Default is "stream"
     * @returns An instance that implements [[ILdpNonRdfSourceDescription]]. If the description exists, the
     *          RDF store of the instance will contain the tripples of the describing LDP RDF Source.
     */
    public async getLDPNonRDFSourceDescription(
        uri: URL,
        acceptContentType: string = "",
        headers: HeadersType = {},
        responseType: SupportedAxiosResponseType = "stream") {
        try {
            const request = this.createAxiosRequest(uri, {
                method: "head",
                responseType,
            }, {
                Accept: acceptContentType,
                ...headers,
            });
            const headResponse = await this.service.request(request);
            const linkHeaderString = headResponse.headers.link || "";
            const linkHeaders = buildLinkHeadersMap(linkHeaderString);
            const describedBy = linkHeaders.getFirstLink(DESCRIBED_BY);
            if (!describedBy) {
                const resource = await this._factory.createBinaryDescriptionResource(undefined, undefined, uri);
                return resource;
            } else {
                const response = await this.httpGetResource<string>(describedBy, headers);
                return this._factory.createBinaryDescriptionResource(response, describedBy, uri);
            }
        } catch (err) {
            this.logger.error("Error while getting LDP NonRDF Source Description: %s", err.toString());
            if (err.response) {
                const response = err.response;
                if (response.status > 400 && response.status < 600) {
                    throw new LdpClientError(response.statusText, response.status, response);
                }
            }
            throw err;
        }
    }

    public async isResourceExistent(uri: URL): Promise<boolean> {
        try {
            const url = new URL(uri.origin);
            url.pathname = uri.pathname;
            await this.service.head(url.toString());
            return true;
        } catch (e) {
            if (e.response.status === 404 || e.response.status === 410) {
                return false;
            } else {
                return e;
            }
        }
    }

    public async isResourceBinary(uri: URL): Promise<boolean> {
        try {
            const url = new URL(uri.origin);
            url.pathname = uri.pathname;
            const head = await this.httpHeadResource(uri, {Accept: ""});
            const linkHeaderString = head.headers.link || "";
            const typeHeaders = buildLinkHeadersMap(linkHeaderString).get(TYPE);
            for (const value of typeHeaders) {
                if (value.toString() === "http://www.w3.org/ns/ldp#NonRDFSource") {
                    return true;
                }
            }
            return false;
        } catch (err) {
            this.logger.error("Error while retieving resource head: %s", err.toString());
            if (err.response) {
                const response = err.response;
                if (response.status > 400 && response.status < 600) {
                    throw new LdpClientError(response.statusText, response.status, response);
                }
            }
            throw err;
        }
    }

    // Create LDP resources

    public async createLDPRDFSource(
        location: URL | ILdpContainer,
        payload: string,
        slug: string,
        headers: HeadersType = {}): Promise<TResource> {
        slug = slug.replace(" ", "-");
        const requestHeaders: any = {
            ...headers,
            Link: '<http://www.w3.org/ns/ldp#RDFSource>; rel="type"',
            Slug: slug,
        };
        location = AxiosLDPClient.getURIFromLocation(location);
        try {
            const response = await this.httpPostResource(location, payload, requestHeaders);
            const createdLocation = response.headers.location || null;
            if (createdLocation) {
                return await this.getResource(new URL(createdLocation));
            } else {
                throw new Error(`No location header found in response for creating ${location.toString()}`);
            }
        } catch (err) {
            this.logger.error("Error while creating LDP Resource: %s", err.toString());
            throw err;
        }
    }

    public async createLDPContainer(
        location: URL | ILdpContainer,
        slug: string,
        headers: HeadersType = {}): Promise<TResource & ILdpContainer> {
        slug = slug.replace(" ", "-");
        const requestHeaders: any = {
            ...headers,
            Link: '<http://www.w3.org/ns/ldp#BasicContainer>; rel="type"',
            Slug: slug,
        };
        location = AxiosLDPClient.getURIFromLocation(location);
        try {
            const response = await this.httpPostResource(location, "", requestHeaders);
            const createdLocation = response.headers.location || null;
            if (createdLocation) {
                return await this.getContainer(new URL(createdLocation));
            } else {
                throw new Error(`No location header found in response after creating ${location.toString()}`);
            }
        } catch (err) {
            this.logger.error("Error while creating LDP Container: %s", err.toString());
            throw err;
        }
    }

    /**
     * Creates a new LDP NonRDF Source in a given LDP Container.
     * @param location An LDP container in which the new LDP NonRDF Source is to be placed.
     * The container can be identified by a container object or directly by a URI.
     * @param slug A hint to the server which identifier should be used for the new LDP NonRDF Source.
     * @param content The binary content of the LDP NonRDF Source. Can be a string or a stream.
     * @param contentType The content-type of the binary content, e.g. "image/png" for a png image.
     * @param headers Optional HTTP-Headers that are additionally included in the HTTP requests
     * @param responseType The response type of the axios response.
     * Use "stream" to get a [[Stream]] or "text" to get a string. Default is "stream"
     * @returns An instance that implements [[ILdpNonRdfSource]] and represents the created LDP NonRDF Source
     */
    public async createLDPNonRDFSource<T = string | Stream>(
        location: ILdpContainer | URL,
        slug: string,
        body: T,
        contentType: string = "application/octet-stream",
        headers: HeadersType = {},
        responseType: SupportedAxiosResponseType = "stream",
        onUploadProgressHandler?: (event: any) => void,
    ): Promise<TResource & ILdpNonRdfSource> {
        slug = slug.replace(" ", "-");
        const requestHeaders: any = {
            ...headers,
            "Accept": contentType,
            "Content-Type": contentType,
            "Link": '<http://www.w3.org/ns/ldp#NonRDFSource>; rel="type"',
            "Slug": slug,
        };
        location = AxiosLDPClient.getURIFromLocation(location);
        try {
            const response = await this.httpPostResource<T>(location, body, requestHeaders, onUploadProgressHandler);
            const createdLocation = response.headers.location || null;
            if (createdLocation) {
                return await this.getLDPNonRDFSource(new URL(createdLocation), contentType, headers, responseType);
            } else {
                throw new Error(`No location header found in response after creating ${location.toString()}`);
            }
        } catch (err) {
            this.logger.error("Error while creating LDP Non RDF Source: %s", err.toString());
            throw err;
        }
    }

    // Update LDP resources
    public async updateLdpRDFSource(ldpResource: ILdpResource, headers: HeadersType = {}): Promise<TResource> {
        const requestHeaders: any = {
            ...headers,
        };
        if (ldpResource.etag) {
            requestHeaders["If-Match"] = ldpResource.strongEtag;
        }
        try {
            await this.httpPutResource(ldpResource.uri, ldpResource.serialize(), requestHeaders);
            return await this.getResource(ldpResource.uri);
        } catch (err) {
            this.logger.error("Error while updating LDP RDF Source: %s", err.toString());
            throw err;
        }
    }

    /**
     * Updates an LDP NonRDF source.
     * @param ldpNonRDFResource The LDP NonRDF Source to be updated
     * @param content The binary content to which the resource should be updated as string or stream
     * @param contentType The content-type of the binary content, e.g. "image/png" for a png image.
     * @param headers Optional HTTP-Headers that are additionally included in the HTTP requests
     * @param responseType The response type of the axios response.
     * Use "stream" to get a [[Stream]] or "text" to get a string. Default is "stream"
     * @returns The updated LDP NonRDF Source
     */
    public async updateLdpNonRDFSource<T = string | Stream>(
        ldpNonRDFResource: ILdpNonRdfSource,
        data: T,
        contentType: string = "application/octet-stream",
        headers: HeadersType = {},
        responseType: SupportedAxiosResponseType = "stream",
    ): Promise<TResource & ILdpNonRdfSource> {

        const requestHeaders: any = {
            ...headers,
            "Content-Type": contentType,
            "Link": '<http://www.w3.org/ns/ldp#NonRDFSource>; rel="type"',
        };

        if (ldpNonRDFResource.etag) {
            requestHeaders["If-Match"] = ldpNonRDFResource.strongEtag;
        }
        try {
            await this.httpPutResource(ldpNonRDFResource.uri, data, requestHeaders);
            return await this.getLDPNonRDFSource(ldpNonRDFResource.uri, contentType, undefined, responseType);
        } catch (err) {
            this.logger.error("Error while updating LDP Non RDF Source: %s", err.toString());
            throw err;
        }
    }

    // Delete LDP resources

    public async deleteLDPResource(uri: URL, headers: HeadersType = {}): Promise<AxiosResponse> {
        try {
            const deleteRequest = this.createAxiosRequest(uri, {
                method: "DELETE",
            }, headers);
            const response = await this.service.request(deleteRequest);
            if (response.status > 400 && response.status < 600) {
                throw new LdpClientError(response.statusText, response.status, response);
            }
            return response;
        } catch (err) {
            this.logger.error("Error during HTTP-Delete: %s", err.toString());
            throw err;
        }
    }

    /**
     * Private helper that extends a request config object of axios with optional headers and the URL of the request
     * @param uri The url of the resource that is the target of the request
     * @param axiosRequestConfig A axios request config object
     * @param optionalHeaders Additional optional headers
     * @returns The extended axios request config object
     */
    protected createAxiosRequest(
        uri: URL,
        axiosRequestConfig: AxiosRequestConfig,
        optionalHeaders?: Record<string, string>,
    ): AxiosRequestConfig {
        const proxyUrl = this._proxyUrlString;
        const proxiedUrl = `${proxyUrl}${uri.toString()}`;
        const headers = {
            ...axiosRequestConfig.headers,
            ...optionalHeaders,
        };
        return {
            url: proxiedUrl,
            ...axiosRequestConfig,
            headers,
        };
    }

    private handleSuccess(response: AxiosResponse<any>) {
        return response;
    }

    private handleError = (error: any) => {
        return Promise.reject(error);
    }
}
